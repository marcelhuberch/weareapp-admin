package ch.weareapp.filter;

import java.util.List;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface FilterRepository extends PagingAndSortingRepository<Filter, Long> {
	List<Filter> findByLabelLikeAllIgnoreCase(String label);
}
