package ch.weareapp.filter;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;

import ch.weareapp.member.Member;
import ch.weareapp.wearegroup.WeAreGroup;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Entity
@Table(name="FILTER")
@Getter
@Setter
@ToString
@NoArgsConstructor
public class Filter implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="id" , nullable = false)
	private Long id;
    
	@Column(nullable = false)
    private String label;
    
    @OneToOne
    private WeAreGroup group;
    
    @JsonIgnore
    //@JsonManagedReference
    @OneToOne
    private Member leader;
    
    @JsonIgnore
    //@JsonBackReference
    @ManyToMany(cascade=CascadeType.ALL, mappedBy="filters")
    private Set<Member> members = new HashSet<Member>();
    
    private Integer sort;
    
    @JsonFormat(pattern = "dd.MM.yyyy, HH:mm:ss")
	private LocalDateTime dateModified = LocalDateTime.now();
	
	private boolean deleted;
	
    public Filter(String label, WeAreGroup group, Integer sort) {
    	this.label = label;
    	this.group = group;
    	this.sort = sort;
    }
	
	public void addMember(Member member) {
		members.add(member);
		member.addFilter(this);
	}
	
	public void removeMember(Member member) {
		members.remove(member);
		member.removeFilter(this);
	}
    
}
