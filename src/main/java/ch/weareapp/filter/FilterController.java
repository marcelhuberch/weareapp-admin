package ch.weareapp.filter;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
@RequestMapping("/filter")
@CrossOrigin(origins = "*")
public class FilterController {

	@Autowired
	private FilterRepository repository;
	
	@RequestMapping(value = "", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> getList(
			@RequestParam(required = false) String q, 
			@RequestParam(required = false) String _sort, 
			@RequestParam(required = false) String _order, 
			@RequestParam(required = false) Integer _start, 
			@RequestParam(required = false) Integer _end) {
		
		List<Filter> filters = new ArrayList<>();
		
		if(StringUtils.hasText(q)) {
			String query = q;
			if ( ! q.contains(" "))
				query = "%" + q + "%";
			filters = repository.findByLabelLikeAllIgnoreCase(query);
		} else if(StringUtils.hasText(_sort) && StringUtils.hasText(_order) && _start != null && _end != null){
			List<Filter> ms = new ArrayList<>();
			Direction direction = "ASC".equals(_order) ? Direction.DESC : Direction.ASC;
			Pageable p = new PageRequest((_end / 10 - 1), (_end - _start), new Sort(direction, _sort));
			Page<Filter> page = repository.findAll(p);
			page.forEach(m -> ms.add(m));
			filters = ms;
		} else {
			List<Filter> ms = new ArrayList<>();
			Iterable<Filter> found = repository.findAll();
			found.forEach(m -> ms.add(m));
			filters = ms;
		}
		
		HttpHeaders responseHeaders = new HttpHeaders();
		responseHeaders.set("X-Total-Count", repository.count() + "");
		responseHeaders.set("Access-Control-Expose-Headers" , "X-Total-Count");
		return new ResponseEntity<List<Filter>>(filters, responseHeaders, HttpStatus.OK);
	}

	@RequestMapping(value = "/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> getById(@PathVariable("id") Long id) {
		Filter filter = repository.findOne(id);
		return ResponseEntity.ok(filter);
	}
	
	@RequestMapping(value = "", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> create(@RequestBody Filter filter) {
		filter = repository.save(filter);
		return ResponseEntity.ok(filter);
	}
	
	@RequestMapping(value = "/{id}", method = RequestMethod.PUT, produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> edit(@RequestBody Filter filter) {
		filter = repository.save(filter);
		return ResponseEntity.ok(filter);
	}
	
	@RequestMapping(value = "/{id}", method = RequestMethod.DELETE, consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> delete(@PathVariable("id") Long id) {
		Filter filter = repository.findOne(id);
		repository.delete(id);
		return ResponseEntity.ok(filter);
	}
}
