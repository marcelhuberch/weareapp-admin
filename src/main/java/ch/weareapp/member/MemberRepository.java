package ch.weareapp.member;

import java.util.List;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface MemberRepository extends PagingAndSortingRepository<Member, Long> {
	List<Member> findByLastnameLikeOrFirstnameLikeAllIgnoreCase(String lastname, String firstname);
}
