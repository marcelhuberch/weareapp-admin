package ch.weareapp.member;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonManagedReference;

import ch.weareapp.filter.Filter;
import ch.weareapp.model.Role;
import ch.weareapp.wearegroup.WeAreGroup;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Entity
@Table(name="MEMBER")
@Getter
@Setter
@ToString
@NoArgsConstructor
@RequiredArgsConstructor
public class Member implements Serializable {
	
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="id" , nullable = false)
	private Long id;
	
	@NonNull
	@Column(nullable = false)
	private String firstname;
	
	@NonNull
	@Column(nullable = false)
	private String lastname;
	
	@Column(unique = true , nullable = false)
	private String username;

	@Column(nullable = false)
	private String password;

	private Role role = Role.USER;
	
	@Column(nullable = false)
	private String function;
	
	private String description;
	
	@OneToOne
	private WeAreGroup group;
	
//	@JsonIgnore
	@JsonManagedReference
	@ManyToMany(cascade=CascadeType.ALL, fetch=FetchType.EAGER)  
    @JoinTable(name="MEMBER_FILTER", joinColumns=@JoinColumn(name="filter_id"), inverseJoinColumns=@JoinColumn(name="member_id")) 
	private Set<Filter> filters = new HashSet<Filter>();
	
	private String image = "https://storage.googleapis.com/weareapp-143217.appspot.com/images/weareapp-demo/defaultImge.gif";
	
	private String street;
	
	private int zip;
	
	private String city;
	
	@Column(unique = true , nullable = false)
	private String email;
	
	private String mobile;

	private String phone;
	
	private String phoneIntern;

	private String contraction;
	
	@JsonFormat(pattern = "dd.MM.yyyy, HH:mm:ss")
	private LocalDateTime dateEntry = LocalDateTime.now();
	
	@JsonFormat(pattern = "dd.MM.yyyy, HH:mm:ss")
	private LocalDateTime dateLeave;
	
	@JsonFormat(pattern = "dd.MM.yyyy, HH:mm:ss")
	private LocalDateTime dateDeleted;
	
	@JsonFormat(pattern = "dd.MM.yyyy, HH:mm:ss")
	private LocalDateTime birthday;
	
	@JsonFormat(pattern = "dd.MM.yyyy, HH:mm:ss")
	private LocalDateTime dateModified = LocalDateTime.now();
	
	private boolean deleted;
	
	public Member(String firstname, String lastname, String username, String password, Role role, WeAreGroup group, String function, String email, String phone, String image) {
		this.firstname = firstname;
		this.lastname = lastname;
		this.username = username;
		this.password = password;
		this.function = function;
		this.role = role;
		this.group = group;
		this.email = email;
		this.phone = phone;
		this.image = image;
	}
	
	public void addFilter(Filter filter) {
		filters.add(filter);
	}
	
	public void removeFilter(Filter filter) {
		filters.remove(filter);
	}
}
