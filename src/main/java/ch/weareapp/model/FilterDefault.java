package ch.weareapp.model;

public enum FilterDefault {

	ALL("All"),
	NEW("New"),
	LEFT("Left WeAreApp");
	
	private String label;
	
	private FilterDefault (String label) {
		this.label = label;
	}

	public String getLabel() {
		return label;
	}
	
	public static boolean isDefaultFilter(String s) {
		if (s != null) {
			if (ALL.getLabel().equals(s) ||
				NEW.getLabel().equals(s) ||
				LEFT.getLabel().equals(s)) {
				return true;
			}
		}
		return false;
	}
}
