package ch.weareapp.model;

import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import ch.weareapp.wearegroup.WeAreGroup;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;

@Entity
@Table(name="PAGE_CONTENT")
@Data
@NoArgsConstructor
@RequiredArgsConstructor
public class PageContent {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="id" , nullable = false)
	private Long id;
    
	@NonNull
    @OneToOne
    private WeAreGroup group;
    
	@NonNull
	@Column(nullable = false)
	private String pagename;
	
	@Column(length = 20000)
	private String content;
	
	@NonNull
	@Column(nullable = false)
	private String locale;
	
	private LocalDateTime dateModified = LocalDateTime.now();
	
	private boolean deleted;
}
