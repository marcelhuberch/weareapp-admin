package ch.weareapp.app;

import java.time.LocalDateTime;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.ApplicationListener;
import org.springframework.stereotype.Component;

import ch.weareapp.filter.Filter;
import ch.weareapp.filter.FilterRepository;
import ch.weareapp.member.Member;
import ch.weareapp.member.MemberRepository;
import ch.weareapp.model.FilterDefault;
import ch.weareapp.model.Role;
import ch.weareapp.utils.Encryptor;
import ch.weareapp.wearegroup.WeAreGroup;
import ch.weareapp.wearegroup.WeAreGroupRepository;

@Component
public class ApplicationStartup implements ApplicationListener<ApplicationReadyEvent> {

	@Autowired
	private WeAreGroupRepository weAreGroupRepository;

	@Autowired
	private MemberRepository memberRepository;
	
	@Autowired
	private FilterRepository filterRepository;

	/**
	 * This event is executed as late as conceivably possible to indicate that
	 * the application is ready to service requests.
	 */
	@Override
	public void onApplicationEvent(final ApplicationReadyEvent event) {

		// Default Filter
		
		Filter filterAll = new Filter(FilterDefault.ALL.getLabel(), null, 1);
		Filter filterNew = new Filter(FilterDefault.NEW.getLabel(), null, 2);
		Filter filterLeft = new Filter(FilterDefault.LEFT.getLabel(), null, 3);
		
		filterRepository.save(filterAll);
		filterRepository.save(filterNew);
		filterRepository.save(filterLeft);
		
		// WeAreApp Demo Group
		WeAreGroup groupDemo = new WeAreGroup("weareapp-demo");
		
//		PageContent pageContentDemo = new PageContent(groupDemo, About.class.getSimpleName(), Locale.ENGLISH.getLanguage());
//		pageContentDemo.setContent("<div class='container'><div class='row'><div class='span12'><img src='https://storage.googleapis.com/weareapp-143217.appspot.com/images/weareapp-demo/logo3.png' class='img-responsive' style='display: block;margin-left: auto;margin-right: auto;'/></div></div></div>"
//				+ "<div class='container'><div class='row'><div class='span12'><p>weareapp demo Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat, vel illum dolore eu feugiat nulla facilisis at vero eros et accumsan et iusto odio dignissim qui blandit praesent luptatum zzril delenit augue duis dolore te feugait nulla facilisi. Lorem ipsum dolor sit amet</p></div></div></div>");
		
		Filter filterHead = new Filter("Head", groupDemo, 20);
		Filter filterDev = new Filter("Development", groupDemo, 30);
		Filter filterTest = new Filter("Testing", groupDemo, 40);
		Filter filterFinance = new Filter("Finance", groupDemo ,50);
		Filter filterSale = new Filter("Sale", groupDemo, 60);
		Filter filterDesign = new Filter("Design", groupDemo, 70);
		Filter filterLaw = new Filter("Law", groupDemo, 80);
		Filter filterRest = new Filter("Restaurant", groupDemo, 90);

		String image = "";
		
		// chuck norris
		image = "https://storage.googleapis.com/weareapp-143217.appspot.com/images/weareapp-demo/chuck.jpg";
		Member chuck = new Member("Chuck", "Norris", "chuck", Encryptor.encrypt("1234"), Role.ADMIN, groupDemo, "Master" , "chuck@email.com", "911", image);
		chuck.setDateEntry(LocalDateTime.now().minusDays(365));
		filterHead.addMember(chuck);
		
		// me
		image = "https://storage.googleapis.com/weareapp-143217.appspot.com/images/weareapp-demo/marcel.jpg";
		Member marcel = new Member("Marcel", "Huber", "marcel", Encryptor.encrypt("1234"), Role.ADMIN, groupDemo, "Java engineer", "marcel.huber@email.com", "+41 12 123 45 67", image);
		marcel.setDescription("Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et.");
		marcel.setStreet("Mainstreed 1");
		marcel.setZip(3000);
		marcel.setCity("Bern");
		marcel.setMobile("+41 79 123 45 67");
		marcel.setPhoneIntern("258");
		marcel.setContraction("MHU");
		marcel.setBirthday(LocalDateTime.of(1975, 04, 25, 0, 0));
		marcel.setDateEntry(LocalDateTime.now().minusDays(365));
		filterDev.addMember(marcel);
		filterTest.addMember(marcel);
		
		// tux
		image = "https://storage.googleapis.com/weareapp-143217.appspot.com/images/weareapp-demo/tux.jpg";
		Member tux = new Member("Tux", "Linux", "tux", Encryptor.encrypt("1234"), Role.USER, groupDemo, "Network engineer", "tux@email.com", "+41 42 142 42 42", image);
		filterDev.addMember(tux);
		
		// dogobert duck
		image = "https://storage.googleapis.com/weareapp-143217.appspot.com/images/weareapp-demo/dagobert.jpg";
		Member dagobert = new Member("Dagobert", "Duck", "duck", Encryptor.encrypt("1234"), Role.USER, groupDemo, "Head Finance", "dduck@email.com", "+41 98 723 45 67", image);
		dagobert.setDateEntry(LocalDateTime.now().minusDays(365));
		filterFinance.addMember(dagobert);

		// adrian Monk
		image = "https://storage.googleapis.com/weareapp-143217.appspot.com/images/weareapp-demo/adrianMonk.png";
		Member adrianMonk = new Member("Adrian", "Monk", "monk", Encryptor.encrypt("1234"), Role.USER, groupDemo, "Testing", "adrianMonk@email.com", "+41 00 157 99 00", image);
		adrianMonk.setDateEntry(LocalDateTime.now().minusDays(365));
		filterTest.addMember(adrianMonk);
		
		// steve jobs
		image = "https://storage.googleapis.com/weareapp-143217.appspot.com/images/weareapp-demo/steveJobs.png";
		Member steveJobs = new Member("Steve", "Jobs", "jobs", Encryptor.encrypt("1234"), Role.USER, groupDemo, "Designer", "steve@email.com", "+41 01 955 20 11", image);
		steveJobs.setDateEntry(LocalDateTime.now().minusDays(365));
		filterDesign.addMember(steveJobs);

		// Howard Lewis Ship
		image = "https://storage.googleapis.com/weareapp-143217.appspot.com/images/weareapp-demo/howardLewisShip.jpg";
		Member howardLewisShip = new Member("Howard Lewis",  "Ship", "ship", Encryptor.encrypt("1234"), Role.USER, groupDemo, "Java engineer", "hls@email.com", "+41 31 234 56 78", image);
		howardLewisShip.setDateEntry(LocalDateTime.now().minusDays(365));
		filterDev.addMember(howardLewisShip);
		
//		// Mr. Bean
//		image = "https://storage.googleapis.com/weareapp-143217.appspot.com/images/weareapp-demo/mrBean.png";
//		Member mrBean = new Member("Bean", "Mr. Bean", "bean", Encryptor.encrypt("1234"), Role.USER, groupDemo, "Salesman", "bean@email.com", "+41 11 222 88 00", image);
//		mrBean.setDateEntry(LocalDateTime.now().minusDays(365));
//		mrBean.addFilter(filterSale);
//		mrBean.addFilter(filterAll);
//		filterSale.addMember(mrBean);

		// McDonalds
		image = "https://storage.googleapis.com/weareapp-143217.appspot.com/images/weareapp-demo/mcDonalds.png";
		Member mcDonalds = new Member("Ronald", "McDonalds", "mcdonalds", Encryptor.encrypt("1234"), Role.USER, groupDemo, "Cook", "mcdonalds@email.com", "+41 33 666 44 00", image);
		mcDonalds.setDateEntry(LocalDateTime.now().minusDays(365));
		filterRest.addMember(mcDonalds);
		
		// charlie chaplin
		image = "https://storage.googleapis.com/weareapp-143217.appspot.com/images/weareapp-demo/charlieChaplin.png";
		Member charlieChaplin = new Member("Charlie", "Chaplin", "chaplin", Encryptor.encrypt("1234"), Role.USER, groupDemo, "Salesman", "cc@email.com", "+41 12 432 00 33", image);
		charlieChaplin.setDateEntry(LocalDateTime.now().minusDays(365));
		filterSale.addMember(charlieChaplin);
		
		// rocky
		image = "https://storage.googleapis.com/weareapp-143217.appspot.com/images/weareapp-demo/rocky.png";
		Member rocky = new Member("Rocky", "Boxer", "rocky", Encryptor.encrypt("1234"), Role.USER, groupDemo, "Lawyer", "rocky@email.com", "+41 33 543 11 00", image);
		rocky.setDateEntry(LocalDateTime.now().minusDays(365));
		filterLaw.addMember(rocky);
		
		// bill gates
		image = "https://storage.googleapis.com/weareapp-143217.appspot.com/images/weareapp-demo/billGates.png";
		Member billGates = new Member("Bill", "Gates", "gates", Encryptor.encrypt("1234"), Role.USER, groupDemo, "Unknown", "bill@email.com", "+41 99 999 99 99", image);
		billGates.setDateEntry(LocalDateTime.now().minusDays(365));
		billGates.setDateLeave(LocalDateTime.now());
				
		// jocker
		image = "https://storage.googleapis.com/weareapp-143217.appspot.com/images/weareapp-demo/jocker.png";
		Member jocker = new Member("Jocker", "Testy", "jocker", Encryptor.encrypt("1234"), Role.USER, groupDemo, "Testing", "jocker@email.com", "+41 13 113 13 13", image);
		filterTest.addMember(jocker);
		
		// albert einstein
		image = "https://storage.googleapis.com/weareapp-143217.appspot.com/images/weareapp-demo/albertEinstein.png";
		Member albertEinstein = new Member("Albert", "Einstein", "einstein", Encryptor.encrypt("1234"), Role.USER, groupDemo, "Scientist", "einstein@email.com", "+41 00 000 00 00", image);
		albertEinstein.setDateEntry(LocalDateTime.now().minusDays(365));
		filterDev.addMember(albertEinstein);
		
		// harry potter
		image = "https://storage.googleapis.com/weareapp-143217.appspot.com/images/weareapp-demo/harryPotter.png";
		Member harryPotter = new Member("Harry", "Potter", "potter", Encryptor.encrypt("1234"), Role.USER, groupDemo, "Testing", "hp@email.com", "+41 24 732 12 00", image);
		filterTest.addMember(harryPotter);
		
		// mark zuckerbert
		image = "https://storage.googleapis.com/weareapp-143217.appspot.com/images/weareapp-demo/markZuckerberg.png";
		Member markZuckerberg = new Member("Mark", "Zuckerberg", "zuckergerg", Encryptor.encrypt("1234"), Role.USER, groupDemo, "PHP Specialist", "facebook@email.com", "+41 12 23 33 55", image);
		markZuckerberg.setDateEntry(LocalDateTime.now().minusDays(365));
		markZuckerberg.setDateLeave(LocalDateTime.now());
		filterDev.addMember(markZuckerberg);

		// angelinaJolie
		image = "https://storage.googleapis.com/weareapp-143217.appspot.com/images/weareapp-demo/angelinaJolie.png";
		Member angelinaJolie = new Member("Angelina", "Jolie", "jolie", Encryptor.encrypt("1234"), Role.USER, groupDemo, "Waitress", "angelinaJolie@email.com", "+41 00 23 27 55", image);
		filterRest.addMember(angelinaJolie);
		
		// gandhi
		image = "https://storage.googleapis.com/weareapp-143217.appspot.com/images/weareapp-demo/gandhi.png";
		Member gandhi = new Member("Gandhi", "Man", "gandhi", Encryptor.encrypt("1234"), Role.USER, groupDemo, "Mentalist", "gandhi@email.com", "+41 12 23 77 55", image);
		gandhi.setDateEntry(LocalDateTime.now().minusDays(365));
		gandhi.addFilter(filterHead);
		filterHead.addMember(gandhi);
		
		// queenElizabeth
		image = "https://storage.googleapis.com/weareapp-143217.appspot.com/images/weareapp-demo/queenElizabeth.png";
		Member queenElizabeth = new Member("Queen", "Elizabeth", "queen", Encryptor.encrypt("1234"), Role.USER, groupDemo, "Investor", "queen@email.com", "+41 12 23 21 55", image);
		queenElizabeth.setDateEntry(LocalDateTime.now().minusDays(365));
		filterFinance.addMember(queenElizabeth);
		
		// sheldonCooper
		image = "https://storage.googleapis.com/weareapp-143217.appspot.com/images/weareapp-demo/sheldonCooper.png";
		Member sheldonCooper = new Member("Sheldon", "Cooper", "cooper", Encryptor.encrypt("1234"), Role.USER, groupDemo, "Smart aleck", "sheldonCooper@email.com", "+41 12 23 33 35", image);
		filterDev.addMember(sheldonCooper);
		
		filterHead.setLeader(chuck);
		filterDev.setLeader(marcel);
		filterTest.setLeader(harryPotter);
		filterFinance.setLeader(queenElizabeth);
		filterSale.setLeader(adrianMonk);
		filterDesign.setLeader(steveJobs);
		filterLaw.setLeader(rocky);
		filterRest.setLeader(mcDonalds);
		
		weAreGroupRepository.save(groupDemo);
//		pageContentDAO.save(pageContentDemo);
		memberRepository.save(chuck);
		memberRepository.save(marcel);
		memberRepository.save(tux);
		memberRepository.save(dagobert);
		memberRepository.save(adrianMonk);
		memberRepository.save(steveJobs);
		memberRepository.save(howardLewisShip);
//		memberRepository.save(mrBean);
		memberRepository.save(mcDonalds);
		memberRepository.save(charlieChaplin);
		memberRepository.save(rocky);
		memberRepository.save(billGates);
		memberRepository.save(jocker);
		memberRepository.save(albertEinstein);
		memberRepository.save(harryPotter);
		memberRepository.save(markZuckerberg);
		memberRepository.save(angelinaJolie);
		memberRepository.save(gandhi);
		memberRepository.save(queenElizabeth);
		memberRepository.save(sheldonCooper);
		filterRepository.save(filterHead);
		filterRepository.save(filterDev);
		filterRepository.save(filterTest);
		filterRepository.save(filterFinance);
		filterRepository.save(filterSale);
		filterRepository.save(filterDesign);
		filterRepository.save(filterLaw);
		filterRepository.save(filterRest);
		
		
//		// ******************* BAND *******************
//		
//		WeAreGroup groupBand = new WeAreGroup("weareapp-band");
////		PageContent pageContentBand = new PageContent(groupBand, About.class.getSimpleName(), Locale.ENGLISH.getLanguage());
////		pageContentBand.setContent("<div class='container'><div class='row'><div class='span12'><img src='https://storage.googleapis.com/weareapp-143217.appspot.com/images/weareapp-demo/logo3.png' class='img-responsive' style='display: block;margin-left: auto;margin-right: auto;'/></div></div></div>"
////				+ "<div class='container'><div class='row'><div class='span12'><p>weareapp band Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat, vel illum dolore eu feugiat nulla facilisis at vero eros et accumsan et iusto odio dignissim qui blandit praesent luptatum zzril delenit augue duis dolore te feugait nulla facilisi. Lorem ipsum dolor sit amet</p></div></div></div>");
//
//		// angusYoung
//		image = "https://storage.googleapis.com/weareapp-143217.appspot.com/images/weareapp-band/angusYoung.jpg";
//		Member angusYoung = new Member("Angus", "Young", "angus", Encryptor.encrypt("acdc"), Role.ADMIN, groupBand, "Lead Guitar" , "angus@acdc.com", "01", image);
//		angusYoung.setDateEntry(LocalDateTime.now().minusDays(365));
//		
//		// bonScott
//		image = "https://storage.googleapis.com/weareapp-143217.appspot.com/images/weareapp-band/bonScott.jpg";
//		Member bonScott = new Member("Bon", "Scott", "bon", Encryptor.encrypt("acdc"), Role.USER, groupBand, "Vocals", "bon@acdc.com", "02", image);
//		bonScott.setDateEntry(LocalDateTime.now().minusDays(365));
//		bonScott.setDateLeave(LocalDateTime.now());
//		
//		// brianJohnson
//		image = "https://storage.googleapis.com/weareapp-143217.appspot.com/images/weareapp-band/brianJohnson.jpg";
//		Member brianJohnson = new Member("Brian", "Johnson", "brian", Encryptor.encrypt("acdc"), Role.USER, groupBand, "Vocals", "brian@acdc.com", "03", image);
//		brianJohnson.setDateEntry(LocalDateTime.now().minusDays(365));
//		
//		// cliffWilliams
//		image = "https://storage.googleapis.com/weareapp-143217.appspot.com/images/weareapp-band/cliffWilliams.jpg";
//		Member cliffWilliams = new Member("Cliff", "Williams", "cliff", Encryptor.encrypt("acdc"), Role.USER, groupBand, "Bass", "cliff@acdc.com", "04", image);
//		cliffWilliams.setDateEntry(LocalDateTime.now().minusDays(365));
//
//		// malcolmYoung
//		image = "https://storage.googleapis.com/weareapp-143217.appspot.com/images/weareapp-band/malcolmYoung.jpg";
//		Member malcolmYoung = new Member("Malcolm", "Young", "malcom", Encryptor.encrypt("acdc"), Role.USER, groupBand, "Rhythm guitar", "malcom@acdc.com", "05", image);
//		malcolmYoung.setDateEntry(LocalDateTime.now().minusDays(365));
//		
//		// philRudd
//		image = "https://storage.googleapis.com/weareapp-143217.appspot.com/images/weareapp-band/philRudd.jpg";
//		Member philRudd = new Member("Phil", "Rudd", "phil", Encryptor.encrypt("acdc"), Role.USER, groupBand, "Drummer", "phil@acdc.com", "06", image);
//		philRudd.setDateEntry(LocalDateTime.now().minusDays(365));
//
//		// axlRose
//		image = "https://storage.googleapis.com/weareapp-143217.appspot.com/images/weareapp-band/axlRose.jpg";
//		Member axlRose = new Member("Axl", "Rose", "axl", Encryptor.encrypt("acdc"), Role.USER, groupBand, "Vocals", "axl@acdc.com", "07", image);
//		axlRose.setDateEntry(LocalDateTime.now());
//		
//		weAreGroupRepository.save(groupBand);
////		pageContentDAO.save(pageContentBand);
//		memberRepository.save(angusYoung);
//		memberRepository.save(bonScott);
//		memberRepository.save(brianJohnson);
//		memberRepository.save(cliffWilliams);
//		memberRepository.save(malcolmYoung);
//		memberRepository.save(philRudd);
//		memberRepository.save(axlRose);
		
		
//		memberRepository.save(new Member("Marcel", "Huber"));
//		memberRepository.save(new Member("Tux", "Linux"));
//		memberRepository.save(new Member("Gary", "Moore"));
//		memberRepository.save(new Member("Joe", "Bonamassa"));
//		memberRepository.save(new Member("Yngwie", "Malmsteen"));
//		memberRepository.save(new Member("Anita", "Huber"));
//		memberRepository.save(new Member("Flux", "Tinux"));
//		memberRepository.save(new Member("Luky", "Luke"));
//		memberRepository.save(new Member("Selina", "Gehrig"));
//		memberRepository.save(new Member("Asterix", "Obelix"));
//		memberRepository.save(new Member("Jürg", "Gehrig"));
//		memberRepository.save(new Member("Mami", "Huber"));
//		memberRepository.save(new Member("Jan", "Huber"));
//		memberRepository.save(new Member("Nico", "Huber"));
//		memberRepository.save(new Member("Irene", "Mary"));
//		memberRepository.save(new Member("Julia", "Mary"));
//		memberRepository.save(new Member("Robert", "Mary"));
//		memberRepository.save(new Member("Eddy", "van Halen"));
	}
}
