package ch.weareapp.wearegroup;

import java.util.List;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface WeAreGroupRepository extends PagingAndSortingRepository<WeAreGroup, Long> {
	List<WeAreGroup> findByNameLikeAllIgnoreCase(String name);
}
