package ch.weareapp.wearegroup;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Entity
@Table(name="WEAREGROUP")
@Getter
@Setter
@ToString
@NoArgsConstructor
@RequiredArgsConstructor
public class WeAreGroup {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="id" , nullable = false)
	private Long id;
	
	@NonNull
	@Column(nullable = false)
	private String name;
	
	private boolean showHasLeft = true;
	
	private int daysIsNew = 30;
	
	private boolean deleted;
}
