import React from 'react';
import ImageField from './imageField';
import FilterField from './filterField';
import { Filter, List,  Edit, Create, EditButton, Datagrid, TextField, DisabledInput, SimpleForm, TextInput} from 'admin-on-rest/lib/mui';

const MemberFilter = (props) => (
    <Filter {...props}>
        <TextInput label="Search" source="q" alwaysOn />
    </Filter>
);

export const MemberList = (props) => (
    <List {...props} filters={<MemberFilter />}>
        <Datagrid>
            <ImageField />
            <TextField source="id" />
            <TextField source="firstname" />
            <TextField source="lastname" />
            <TextField source="email" />
            <FilterField label="Filters"/>
            <EditButton basePath="/member" />
        </Datagrid>
    </List>
);

const MemberTitle = ({ record }) => {
    return <span>Member {record ? `"${record.firstname} ${record.lastname}"` : ''}</span>;
};

export const MemberEdit = (props) => (
    <Edit title={<MemberTitle />} {...props}>
        <SimpleForm>
            <DisabledInput source="id" />
            <TextInput source="firstname" />
            <TextInput source="lastname" />
        </SimpleForm>
    </Edit>
);

export const MemberCreate = (props) => (
    <Create {...props}>
        <SimpleForm>
            <TextInput source="firstname" />
            <TextInput source="lastname" />
        </SimpleForm>
    </Create>
);
