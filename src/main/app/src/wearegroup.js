import React from 'react';
import { Filter, List,  Edit, Create, EditButton, Datagrid, TextField, DisabledInput, SimpleForm, TextInput } from 'admin-on-rest/lib/mui';

const WearegroupFilter = (props) => (
    <Filter {...props}>
        <TextInput label="Search" source="q" alwaysOn />
    </Filter>
);

export const WearegroupList = (props) => (
    <List {...props} filters={<WearegroupFilter />}>
        <Datagrid>
            <TextField source="id" />
            <TextField source="name" />
            <EditButton basePath="/wearegroup" />
        </Datagrid>
    </List>
);

const WearegroupTitle = ({ record }) => {
    return <span>Wearegroup {record ? `"${record.name}"` : ''}</span>;
};

export const WearegroupEdit = (props) => (
    <Edit title={<WearegroupTitle />} {...props}>
        <SimpleForm>
            <DisabledInput source="id" />
            <TextInput source="name" />
        </SimpleForm>
    </Edit>
);

export const WearegroupCreate = (props) => (
    <Create {...props}>
        <SimpleForm>
            <TextInput source="name" />
        </SimpleForm>
    </Create>
);
