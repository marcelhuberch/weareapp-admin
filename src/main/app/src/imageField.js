import React from 'react';

const ImageField = ({ record }) =>
    <img src={`${record.image}`} height="40px" width="40px" alt={`${record.firstname}`} />;

ImageField.defaultProps = {
    size: 40,
};

export default ImageField;
