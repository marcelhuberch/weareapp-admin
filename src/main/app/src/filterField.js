import React from 'react';
import Chip from 'material-ui/Chip';

const styles = {
    main: { display: 'flex', flexWrap: 'wrap' },
    chip: { margin: 4 },
};

const FilterField = ({ record }) => (
    <span style={styles.main}>
        {record.filters.map(f => (
                <Chip key={f.id} style={styles.chip}>
                      {f.label}
                </Chip>
        ))}
    </span>
);
export default FilterField;
