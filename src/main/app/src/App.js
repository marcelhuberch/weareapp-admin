import React from 'react';
import WearegroupIcon from 'material-ui/svg-icons/social/domain';
import MemberIcon from 'material-ui/svg-icons/social/group';
import FilterIcon from 'material-ui/svg-icons/content/filter-list';
import { jsonServerRestClient, Admin, Resource } from 'admin-on-rest';
import { Delete } from 'admin-on-rest/lib/mui';
import { WearegroupList , WearegroupEdit , WearegroupCreate } from './wearegroup';
import { MemberList , MemberEdit , MemberCreate } from './member';
import { FilterList , FilterEdit , FilterCreate } from './filter';

const App = () => (
    <Admin title="WeAreApp Admin" restClient={jsonServerRestClient('http://localhost:8080')}>
        <Resource name="wearegroup" list={WearegroupList} edit={WearegroupEdit} create={WearegroupCreate} remove={Delete} icon={WearegroupIcon} />
        <Resource name="member" list={MemberList} edit={MemberEdit} create={MemberCreate} remove={Delete} icon={MemberIcon} />
        <Resource name="filter" list={FilterList} edit={FilterEdit} create={FilterCreate} remove={Delete} icon={FilterIcon} />
    </Admin>
);

export default App;
