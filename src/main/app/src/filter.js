import React from 'react';
import { Filter, List,  Edit, Create, EditButton, Datagrid, TextField, DisabledInput, SimpleForm, TextInput } from 'admin-on-rest/lib/mui';

const FilterFilter = (props) => (
    <Filter {...props}>
        <TextInput label="Search" source="q" alwaysOn />
    </Filter>
);

export const FilterList = (props) => (
    <List {...props} filters={<FilterFilter />}>
        <Datagrid>
            <TextField source="id" />
            <TextField source="label" />
            <EditButton basePath="/filter" />
        </Datagrid>
    </List>
);

const FilterTitle = ({ record }) => {
    return <span>Filter {record ? `"${record.label}"` : ''}</span>;
};

export const FilterEdit = (props) => (
    <Edit title={<FilterTitle />} {...props}>
        <SimpleForm>
            <DisabledInput source="id" />
            <TextInput source="label" />
        </SimpleForm>
    </Edit>
);

export const FilterCreate = (props) => (
    <Create {...props}>
        <SimpleForm>
            <TextInput source="label" />
        </SimpleForm>
    </Create>
);
